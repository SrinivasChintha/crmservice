package com.allstate.controllers.customer;

import com.allstate.models.auth.ERole;
import com.allstate.models.auth.Role;
import com.allstate.models.auth.User;
import com.allstate.models.common.Address;
import com.allstate.models.common.Name;
import com.allstate.models.customer.Customer;
import com.allstate.payload.auth.request.LoginRequest;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.payload.auth.response.JwtResponse;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(properties={"spring.data.mongodb.database=CEMESystemDBTests","spring.data.mongodb.host=127.0.0.1"})
public class CustomerControllerITTests {

    @LocalServerPort
    private int port;

    @Autowired
    MongoTemplate template;

    @Autowired
    private TestRestTemplate restTemplate;

    private String basePath = "/api/v1/customer";

    private Customer customer = new Customer(
            new Name() {
            },
            "+91 6987989987",
            "v.c@gmail.com",
            new Address() {
            }
    );

    private Role role;
    private SignupRequest signupRequest;
    private String authToken;
    private HttpHeaders headers;

    @BeforeAll
    public void signUpUser() {
        role = new Role();
        role.setId("5fc4a002568b11865b70e8df");
        role.setName(ERole.ROLE_ADMIN);
        template.save(role);

        signupRequest = new SignupRequest();
        signupRequest.setEmail("admin@gmail.com");
        signupRequest.setFullName("Admin Test");
        signupRequest.setPassword("admin12345");
        signupRequest.setUsername("admin");
        signupRequest.setRole(new HashSet<String>(Arrays.asList("admin")));
        this.restTemplate.postForObject("http://localhost:" + port + "/api/v1/user/signup",
                signupRequest, Object.class);

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(signupRequest.getUsername());
        ;
        loginRequest.setPassword(signupRequest.getPassword());
        JwtResponse jwtResponse = this.restTemplate.postForObject("http://localhost:" + port + "/api/v1/user/signin",
                loginRequest, JwtResponse.class);
        authToken = "Bearer " + jwtResponse.getAccessToken();

        headers = new HttpHeaders();
        headers.add("authorization", authToken);
    }

    @Test
    @Order(1)
    public void saveCustomer() throws Exception {
        HttpEntity<Customer> customerHttpEntity = new HttpEntity(customer, headers);
        ResponseEntity<String> id = this.restTemplate.postForEntity("http://localhost:" + port + basePath + "/save",
                customerHttpEntity, String.class);
        customer.setId(new ObjectId(id.getBody()));
        assertThat(id.getBody()).isNotBlank();
    }

    @Test
    @Order(2)
    public void updateCustomer() throws Exception {
        HttpEntity<Customer> customerHttpEntity = new HttpEntity(customer, headers);
        assertThat(this.restTemplate.postForEntity("http://localhost:" + port + basePath + "/update",
                customerHttpEntity, long.class).getBody()).isEqualTo(1);
    }

    @Test
    @Order(3)
    public void getAllCustomers() throws Exception {
        HttpEntity<Customer> httpEntity = new HttpEntity(headers);
        ResponseEntity<? extends ArrayList> response
                = this.restTemplate.exchange("http://localhost:" + port + basePath + "/all",
                HttpMethod.GET, httpEntity, new ArrayList<Customer>().getClass());
        assertThat(response.getBody().toArray()).isNotEmpty();
    }

    @Test
    @Order(4)
    public void findCustomer() throws Exception {
        HttpEntity<Customer> httpEntity = new HttpEntity(headers);
        ResponseEntity<Customer> response
                = this.restTemplate.exchange("http://localhost:" + port + basePath + "/" + customer.getId().toHexString(),
                HttpMethod.GET, httpEntity, Customer.class);
        assertThat(response.getBody()).isNotNull();
    }

    @AfterAll
    public void cleanup() {
        template.dropCollection(Customer.class);
        template.dropCollection(User.class);
        template.dropCollection(Role.class);
    }
}
