package  com.allstate.controllers.user;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.LoginRequest;
import com.allstate.payload.auth.request.SignupRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserControllerITest{

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MongoTemplate mongoTemplate;

    @BeforeEach
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @WithMockUser("Administrator")
    @Test
    @Order(1)
    public void testRegisterUser() throws  Exception{
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("testuser123");
        signUpRequest.setFullName("testuser123");
        signUpRequest.setPassword("testuser123");
        signUpRequest.setEmail("testuser123@gmail.com");
        String jsonRequest = objectMapper.writeValueAsString(signUpRequest);

        MvcResult result = mockMvc.perform(post("/api/v1/user/signup").content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }
    @WithMockUser("Administrator")
    @Test
    @Order(2)
    public void testGetAllUser() throws  Exception{
        MvcResult result = mockMvc.perform(get("/api/v1/user/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    @Order(3)
    public void testAuthenticateUser() throws  Exception{
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("testuser123");
        loginRequest.setPassword("testuser123");
        String jsonRequest = objectMapper.writeValueAsString(loginRequest);

        MvcResult result = mockMvc.perform(post("/api/v1/user/signin").content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @AfterAll
    public  void cleanup(){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is("testuser123"));
        mongoTemplate.findAndRemove(query, User.class);
    }


}