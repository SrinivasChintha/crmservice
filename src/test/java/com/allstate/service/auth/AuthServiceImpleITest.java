package com.allstate.service.auth;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.LoginRequest;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.service.user.UserService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthServiceImpleITest {

    private LoginRequest loginRequest = new LoginRequest();

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setup(){
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("testuser123");
        signUpRequest.setFullName("testuser123");
        signUpRequest.setPassword("testuser123");
        signUpRequest.setEmail("testuser123@gmail.com");
        userService.register(signUpRequest);

        loginRequest.setPassword("testuser123");
        loginRequest.setUsername("testuser123");
    }

    @Test
    public void testAuthenticate(){
        ResponseEntity<?> object = authService.authenticate(loginRequest);
        assertEquals(HttpStatus.OK, object.getStatusCode());

    }

    @AfterAll
    public void cleanup(){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is("testuser123"));
        mongoTemplate.findAndRemove(query, User.class);
    }
}
