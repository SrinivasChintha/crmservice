package com.allstate.repository.auth;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.service.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserRepositoryITest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;

    private SignupRequest signupRequest;

    @Autowired
    UserService userService;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setUp(){
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("testuser123");
        signUpRequest.setFullName("testuser123");
        signUpRequest.setPassword("testuser123");
        signUpRequest.setEmail("testuser123@gmail.com");
        userService.register(signUpRequest);
    }

    @Test
    public void testFindByUsername(){
        Optional<User> user= userRepository.findByUsername("testuser123");
        assertNotNull(user);
    }

    @Test
    public void testExistsByUsername(){
        boolean isExist = userRepository.existsByUsername("testuser123");
        assertTrue(isExist);
    }

    @Test
    public void testExistsByEmail(){
        boolean isExist = userRepository.existsByEmail("testuser123@gmail.com");
        assertTrue(isExist);
    }

    @AfterAll
    public void cleanup(){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is("testuser123"));
        mongoTemplate.findAndRemove(query,User.class);
    }
}
