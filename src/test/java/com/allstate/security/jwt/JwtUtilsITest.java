package com.allstate.security.jwt;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.service.user.UserService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JwtUtilsITest {

    private Authentication authentication;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserService userService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    MongoTemplate mongoTemplate;

    private String token;

    @BeforeEach
    public void setup(){
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("testuser123");
        signUpRequest.setFullName("testuser123");
        signUpRequest.setPassword("testuser123");
        signUpRequest.setEmail("testuser123@gmail.com");
        userService.register(signUpRequest);

        authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken("testuser123", "testuser123"));
    }

    @Test
    public void testGenerateJwtToken(){
        token =  jwtUtils.generateJwtToken(authentication);
       assertNotNull(token);
    }

    @Test
    public void testGetUserNameFromJwtToken(){
        String userName= jwtUtils.getUserNameFromJwtToken(token);
        assertNotNull(userName);
        assertEquals("testuser123",userName);
    }

    @Test
    public void testValidateJwtToken(){
        boolean isValid = jwtUtils.validateJwtToken(token);
        assertTrue(isValid);
    }

    @AfterAll
    public void cleanup(){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is("testuser123"));
        mongoTemplate.findAndRemove(query, User.class);
    }
}
