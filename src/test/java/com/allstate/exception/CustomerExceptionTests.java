package com.allstate.exception;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerExceptionTests {

    private Exception exception;

    @BeforeAll
    public void setUp() {
        exception = new Exception("Error !");
    }

    @Test
    public void withMessage() {
        CustomerException customerException = new CustomerException("Error message !");
        assertEquals(customerException.getMessage(), "Error message !");
    }

    @Test
    public void withException() {
        CustomerException customerException = new CustomerException(exception);
        assertEquals(customerException.getCause(), exception);
    }

    @Test
    public void withMessageAndException() {
        CustomerException customerException = new CustomerException("Error message !", exception);
        assertEquals(customerException.getMessage(), "Error message !");
        assertEquals(customerException.getCause(), exception);
    }
}
