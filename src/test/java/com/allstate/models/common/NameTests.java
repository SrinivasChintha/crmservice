package com.allstate.models.common;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NameTests {

    private Name name;

    @BeforeAll
    void setUp() throws Exception {
        name = new Name();
        name.setFirstName("First Name");
        name.setLastName("Last Name");
    }

    @Test
    void getTests() {
        assertEquals("First Name", name.getFirstName());
        assertEquals("Last Name", name.getLastName());
    }
}
