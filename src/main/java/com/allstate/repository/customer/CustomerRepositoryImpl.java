package com.allstate.repository.customer;

import com.allstate.models.customer.Customer;
import com.mongodb.client.result.UpdateResult;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements  CustomerRepository {

    @Autowired
    private MongoTemplate template;

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> customers = template.findAll(Customer.class);
        return customers;
    }

    @Override
    public Customer findCustomer(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(new ObjectId(id)));
        Customer customer = template.findOne(query, Customer.class);
        return customer;
    }

    @Override
    public String saveCustomer(Customer customer) {
        customer = template.save(customer);
        return customer.getId().toHexString();
    }

    @Override
    public long updateCustomer(Customer customer) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(customer.getId()));
        Update update = new Update();
        update.set("name", customer.getName());
        update.set("phoneNumber", customer.getPhoneNumber());
        update.set("email", customer.getEmail());
        update.set("address", customer.getAddress());

        UpdateResult updateResult = template.updateFirst(query, update, Customer.class);
        return  updateResult.getMatchedCount();
    }
}
