package com.allstate.repository.interaction;

import com.allstate.models.interaction.Interaction;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class InteractionRepositoryImpl implements InteractionRepository {

    @Autowired
    MongoTemplate tpl;

    @Override
    public void saveInteraction(Interaction interaction) {
        tpl.save(interaction);
    }

    @Override
    public List<Interaction> getAllInteractions() {

        return tpl.findAll(Interaction.class);
    }

    @Override
    public List<Interaction> getUserInteractions(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        return tpl.find(query, Interaction.class);
    }

    @Override
    public void deleteInteraction(String id) {
        tpl.remove(new Query(Criteria.where("id").is(id)), Interaction.class);
    }

    @Override
    public void updateInteraction(Interaction interaction) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(interaction.getId()));
        Update update = new Update();
        update.set("userId", interaction.getUserId());
        update.set("subject", interaction.getSubject());
        update.set("creationDate", interaction.getCreationDate());
        update.set("custName", interaction.getCustName());
        update.set("closeDate", interaction.getCloseDate());
        update.set("comments", interaction.getComments());
        update.set("status", interaction.getStatus());

        tpl.updateFirst(query, update, Interaction.class);

    }
}
