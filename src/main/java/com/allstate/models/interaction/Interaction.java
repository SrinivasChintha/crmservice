package com.allstate.models.interaction;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
@Document
public class Interaction {
    @Id
    private String id;
    private String custName;
    private String userId;
    private String subject;
    private String creationDate;
    private String closeDate;
    private InteractionStatus status;
    private  String comments;

    public Interaction(){

    }
    public Interaction(String custName, String userId, String subject, String creationDate,
                        String closeDate, InteractionStatus status, String comments){
        this.custName = custName;
        this.userId = userId;
        this.subject = subject;
        this.creationDate = creationDate;
        this.closeDate = closeDate;
        this.status = status;
        this.comments = comments;
    }
    public String getId(){return id;}


    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public InteractionStatus getStatus() {
        return status;
    }

    public void setStatus(InteractionStatus status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Interaction{" +
                "id=" + id +
                ", custName=" + custName +
                ", UserId='" + userId +
                ", subject=" + subject +
                ", status=" + status +
                ", Comment=" + comments+
                '}';
    }
}
