package com.allstate.models.common;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Address {

    @NotBlank
    @Size(max = 50)
    private String buildingNumberAndName;

    @NotBlank
    @Size(max = 50)
    private String streetName;

    @Size(max = 50)
    private String locality;

    @NotBlank
    @Size(max = 50)
    private String city;

    @NotBlank
    @Size(max = 50)
    private String state;

    @NotBlank
    @Size(min = 6, max = 6)
    private String zipCode;

    public String getBuildingNumberAndName() {
        return buildingNumberAndName;
    }

    public void setBuildingNumberAndName(String buildingNumberAndName) {
        this.buildingNumberAndName = buildingNumberAndName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Address() {
    }

    public Address(String buildingNumberAndName, String streetName, String locality, String city, String state, String zipCode) {
        this.buildingNumberAndName = buildingNumberAndName;
        this.streetName = streetName;
        this.locality = locality;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }
}
