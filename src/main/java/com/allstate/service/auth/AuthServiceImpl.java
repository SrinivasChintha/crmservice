package com.allstate.service.auth;

import com.allstate.exception.UserNotFoundException;
import com.allstate.models.auth.ERole;
import com.allstate.models.auth.Role;
import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.LoginRequest;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.payload.auth.response.JwtResponse;
import com.allstate.payload.auth.response.MessageResponse;
import com.allstate.repository.auth.RoleRepository;
import com.allstate.repository.auth.UserRepository;
import com.allstate.security.jwt.JwtUtils;
import com.allstate.security.services.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService{
    private static final String CLASS_NAME = AuthServiceImpl.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AuthService authService;

    @Autowired
    JwtUtils jwtUtils;


    @Override
    public ResponseEntity<?> authenticate(LoginRequest loginRequest) {
        final String METHOD_NAME = "authenticate()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        }catch(BadCredentialsException badCredentialsException){
            throw new UserNotFoundException("Incorrect Username & Password",badCredentialsException);
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("User tried to login :", loginRequest.getUsername());
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());
        LOGGER.info("Exited from "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getFullName(),
                roles));
    }
}
