package com.allstate.service.customer;

import com.allstate.exception.CustomerException;
import com.allstate.models.customer.Customer;
import com.allstate.repository.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getAllCustomers() {

        try {
            return customerRepository.getAllCustomers();
        } catch (Exception ex) {
            throw new CustomerException(ex);
        }
    }

    @Override
    public Customer findCustomer(String id) {
        try {
            return customerRepository.findCustomer(id);
        } catch (Exception ex) {
            throw new CustomerException(ex);
        }
    }

    @Override
    public String saveCustomer(Customer customer) {
        try {
            return customerRepository.saveCustomer(customer);
        } catch (Exception ex) {
            throw new CustomerException(ex);
        }
    }

    @Override
    public long updateCustomer(Customer customer) {
        try {
            return customerRepository.updateCustomer(customer);
        } catch (Exception ex) {
            throw new CustomerException(ex);
        }
    }
}
