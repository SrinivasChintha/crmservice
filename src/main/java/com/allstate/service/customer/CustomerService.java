package com.allstate.service.customer;

import com.allstate.models.customer.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();

    Customer findCustomer(String id);

    String saveCustomer(Customer customer);

    long updateCustomer(Customer customer);
}
