package com.allstate.service.interaction;

import com.allstate.exception.InteractionException;
import com.allstate.models.interaction.Interaction;
import org.bson.types.ObjectId;

import java.util.List;

public interface InteractionService {
    List<Interaction> getAllInteractions();

    List<Interaction> getUserInteractions(String userId);

    void save(Interaction interaction) throws InteractionException;

    void deleteInteraction(String id);
    void updateInteraction(Interaction interaction);
}
