package com.allstate.controllers.interaction;

import com.allstate.controllers.customer.CustomerController;
import com.allstate.models.interaction.Interaction;
import com.allstate.service.interaction.InteractionService;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/interaction")
public class InteractionController {

    private static final String CLASS_NAME = CustomerController.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

        @Autowired
        InteractionService service;
        @RequestMapping( value = "/getall", method = RequestMethod.GET)
        @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
        public List<Interaction> all() {
            LOGGER.info("Method getAllInteraction called in " + CLASS_NAME);
try{
        return service.getAllInteractions();
    }
catch(Exception ex){
    LOGGER.error("Exception calling getAllInteraction", ex);
    return null;
}}
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
        @RequestMapping( value = "/save", method = RequestMethod.POST )
        public void save(@RequestBody Interaction interaction){
        LOGGER.info("success calling save Interaction" + CLASS_NAME);
try{
        service.save(interaction);
    }
catch(Exception ex) {
    LOGGER.error("Exception calling getAllInteraction", ex);
}}

    @RequestMapping( value = "/getbyuser/{userId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List> getUserInteraction(@PathVariable("userId") String userId) {
        try {
            List<Interaction> userInteractionList = service.getUserInteractions(userId);
            LOGGER.info("success calling getuserByID " + CLASS_NAME);
            if (userInteractionList != null && userInteractionList.size() == 0) {
                return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List>(userInteractionList, HttpStatus.OK);
            }
        } catch (Exception ex) {
            LOGGER.error("Exception calling getAlluser", ex);
            return null;
        }
    }

    @RequestMapping( value = "/delete/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public void deleteInteraction(@PathVariable("id") String id) {
try{
            service.deleteInteraction(id);

    LOGGER.info("success calling delete By ID " + CLASS_NAME);
    }catch(Exception ex) {

    LOGGER.error("exception calling delete By ID in Interaction ", ex);
}    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @RequestMapping( value = "/update", method = RequestMethod.POST )
    public void updateInteraction(@RequestBody Interaction interaction){
try{
        service.updateInteraction(interaction);

    LOGGER.info("success calling Update " + CLASS_NAME);
    }catch(Exception ex) {

    LOGGER.error("Exception calling update Interaction " + CLASS_NAME);
}
    }
    }