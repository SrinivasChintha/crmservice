package com.allstate.exception;

public class CustomerException extends RuntimeException {
    public CustomerException(String message) {
        super(message);
    }

    public CustomerException(Exception exception) {
        super(exception.getMessage(), exception);
    }

    public CustomerException(String message, Exception exception) {
        super(message, exception);
    }
}
