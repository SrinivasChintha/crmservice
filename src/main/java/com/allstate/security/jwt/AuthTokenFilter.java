package com.allstate.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.allstate.security.services.UserDetailsServiceImpl;

public class AuthTokenFilter extends OncePerRequestFilter {
    private static final String CLASS_NAME = AuthTokenFilter.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        final String METHOD_NAME = "doFilterInternal()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);

            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                String username = jwtUtils.getUserNameFromJwtToken(jwt);
                if(username!=null &&  SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null,
                            userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }

        filterChain.doFilter(request, response);
        LOGGER.info("Exited from "+CLASS_NAME+ "  method:"+ METHOD_NAME);
    }

    private String parseJwt(HttpServletRequest request) {
        final String METHOD_NAME = "parseJwt()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);

        String headerAuth = request.getHeader("Authorization");

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Authorization token sent by user:", headerAuth);
        }

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }
        LOGGER.info("Exited from "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        return null;
    }
}