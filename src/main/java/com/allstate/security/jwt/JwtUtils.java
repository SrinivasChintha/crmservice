package com.allstate.security.jwt;


import com.allstate.security.services.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtUtils {
    private static final String CLASS_NAME = JwtUtils.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${allstate.app.jwtSecret}")
    private String jwtSecret;

    @Value("${allstate.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    public String generateJwtToken(Authentication authentication) {

        final String METHOD_NAME = "generateJwtToken()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getUserNameFromJwtToken(String token) {
        final String METHOD_NAME = "getUserNameFromJwtToken()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);

        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken) {
        final String METHOD_NAME = "validateJwtToken()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);

        if (LOGGER.isDebugEnabled())
            LOGGER.debug("Authentication token: ", authToken);

        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            LOGGER.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            LOGGER.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            LOGGER.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            LOGGER.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error("JWT claims string is empty: {}", e.getMessage());
        }
        LOGGER.info("Exited from "+CLASS_NAME+ "  method:"+ METHOD_NAME);

        return false;
    }
}

